from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class Driver(object):
    phantom = None

    """docstring for Driver."""
    def __init__(self):
        super(Driver, self).__init__()
        user_agent = (
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) " +
            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36"
        )
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = user_agent

        self.phantom = webdriver.PhantomJS(desired_capabilities=dcap)
        self.phantom.set_script_timeout(15)
        self.phantom.set_window_size(1280, 1024)

    def guardarScreenshot(self,nombre):
        self.phantom.save_screenshot("screenshots/"+nombre+'.png')

    def enviarQuerys(self,texto):
        self.phantom.find_element_by_name('raw-tab').click()
        self.phantom.find_element_by_id('id__content').clear()
        self.phantom.find_element_by_id('id__content').send_keys(texto)
        self.guardarScreenshot('prueba')
        btn = self.phantom.find_elements_by_xpath('//button')
        for unBtn in btn:
            if unBtn.text == 'POST':
                unBtn.click()

    def cargarPagina(self,url):
        self.phantom.get(url)

    def esperar(self,segundos):
        self.phantom.implicitly_wait(segundos)

    def esperarRespuesta(self):
        try:
            element = WebDriverWait(self.phantom, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "response-info"))
            )
        except Exception as e:
            print str(e)