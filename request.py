from db import *

class Request(object):
    request = None
    urlsEnviadas = None
    db = None

    """docstring for Request."""
    def __init__(self):
        super(Request, self).__init__()
        self.request = '{"id_proyecto": 1,"nombre_directorio": "leanSelenium","claves": [{"id": 211,"clave": "Tea"},{"id": 212,"clave": "comprar AND Tea AND Sudamerica AND Maquinaries -coffee"}]}'
        self.urlsEnviadas = self.crearUrls()

    def crearUrls(self):
        self.db = Database()
        idRequest = self.db.obtenerIdRequest()
        urls = {}
        urls['id_proyecto'] = 5
        urls['nombre_directorio'] = "leanSelenium" + str(idRequest)
        urls['request'] = idRequest
        urls['urls'] = list()

        file = open('urls/enlaces.txt')
        for index,unaLinea in enumerate(file.readlines()):
            unaLinea = unaLinea.split("\n")[0]
            unaUrls = {}
            unaUrls['orden'] = index + 1
            unaUrls['url'] = unaLinea
            urls['urls'].append(unaUrls)

        return urls
