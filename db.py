import MySQLdb

class Database(object):
    host = ""
    user = ""
    passwd = ""
    dbname = ""
    db = None
    cur = None
    """docstring for Database."""
    def __init__(self):
        super(Database, self).__init__()
        self.db = MySQLdb.connect(host=self.host,user=self.user,passwd=self.passwd,db=self.dbname)
        self.cur = self.db.cursor()

    def obtenerUltimoIdRequest(self):
        self.cur.execute("SELECT * FROM searchkeyws_wsrequest ORDER BY id as DESC limit 1")
        for row in self.cur.fetchall():
            print row[0]