import json

from driver import *
from request import *


def main():
    driver = Driver()
    driver.cargarPagina('http://localhost:8080/api/wsrequests/')

    unJson = Request()
    driver.enviarQuerys(unJson.request)
    driver.esperar(5)
    driver.guardarScreenshot('final2')


    driver.cargarPagina('http://localhost:8080/api/wsfilteredurlsrequests/')
    driver.enviarQuerys(json.dumps(unJson.urlsEnviadas))


if __name__ == "__main__":
    main()



